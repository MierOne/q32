package itis.q32;


/*
В папке resources находятся два .csv файла.
Один содержит данные о группах в университете в следующем формате: ID группы, название группы, код группы
Второй содержит данные о студентах: ФИО, дата рождения, айди группы, количество очков рейтинга

напишите код который превратит содержимое файлов в обьекты из пакета "entities", выведите в консоль всех студентов,
в читабельном виде, с информацией о группе
Используя StudentService, выведите:

1. Число групп с только совершеннолетними студентами
2. Самую маленькую группу
3. Отношение группа - сумма балов студентов фамилия которых совпадает с заданной строкой
4. Отношения студент - дельта баллов до проходного порога (порог передается параметром),
 сгруппированные по признаку пройден порог, или нет

Требования к реализации: все методы в StudentService должны быть реализованы с использованием StreamApi.
Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
*/

import itis.q32.entities.Group;
import itis.q32.entities.Student;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class MainClass {

    private StudentService studentService = new StudentServiceImpl();

    public static void main(String[] args) throws IOException {
        new MainClass().run(
                "src\\itis\\q32\\resources\\students.csv",
                "src\\itis\\q32\\resources\\groups.csv");
    }

    private void run(String studentsPath, String groupsPath) throws IOException {
        ArrayList<String> groupsLines = readCSVLines(groupsPath);
        ArrayList<String> studentsLines = readCSVLines(studentsPath);
        ArrayList<Group> groups = new ArrayList<>();
        ArrayList<Student> students = new ArrayList<>();

        for (int i = 0; i < groupsLines.size(); i++) {
            groups.add(parseLineToGroupObject(groupsLines.get(i)));
            System.out.println(groups.get(i));
        }

        for (int i = 0; i < studentsLines.size(); i++) {
            students.add(parseLineToStudentObject(studentsLines.get(i), groups));
            System.out.println(students.get(i));
        }

        System.out.println(studentService.countGroupsWithOnlyAdultStudents(students));


        System.out.println(studentService.getGroupScoreSumMap(students, "Смит"));


    }

    public ArrayList<String> readCSVLines(String path) throws IOException {
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        ArrayList<String> list = new ArrayList<>();
        String str = bufferedReader.readLine();
        while(str!=null){
            list.add(str);
            str = bufferedReader.readLine();
        }

        bufferedReader.close();
        fileReader.close();

        return list;
    }

    public Group parseLineToGroupObject(String str){
        String[] strings = str.split(", ");
        Group group = new Group();
        group.setId(Long.parseLong(strings[0]));
        group.setTitle(strings[1]);
        group.setCode(strings[2]);

        return group;
    }

    public Student parseLineToStudentObject(String str, ArrayList<Group> groups){
        String[] strings = str.split(", ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
        Student student = new Student();
        student.setFullName(strings[0]);
        student.setBirthdayDate(LocalDate.parse(strings[1], formatter));
        Long x = Long.parseLong(strings[2]);
        for (int i = 0; i < groups.size(); i++) {
            if(groups.get(i).getId().equals(x)){
                student.setGroup(groups.get(i));
                break;
            }
        }
        student.setScore(Integer.parseInt(strings[3]));

        return student;
    }
}










