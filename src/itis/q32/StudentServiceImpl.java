package itis.q32;

import itis.q32.entities.Group;
import itis.q32.entities.Student;
import sun.util.resources.LocaleData;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StudentServiceImpl implements StudentService {


    @Override
    public Group getSmallestGroup(List<Student> students) {
        for (int i = 0; i < students.size(); i++) {

        }
        return null;
    }

    @Override
    public Integer countGroupsWithOnlyAdultStudents(List<Student> students) {
        LocalDate localDate1 = LocalDate.now();
        return (int) students.stream()
                .filter(o -> {
                    LocalDate d = o.getBirthdayDate();
                    return Period.between(d, localDate1).getYears()>=18;
                }).count();
    }

    @Override
    public Map<String, Integer> getGroupScoreSumMap(List<Student> students, String studentSurname) {
        return students.stream().filter(o -> o.getFullName().contains(studentSurname))
                .collect(Collectors.toMap(o -> o.getFullName(), a -> a.getScore()));
    }

    @Override
    public Map<Boolean, Map<String, Integer>> groupStudentScoreWithThreshold(List<Student> students, Integer threshold) {
        //Map<Boolean, Map<String, Integer>> map = students.stream().collect(Collectors.toMap(o->o.getScore()-threshold>0, students.stream()
        //.collect(Collectors.toMap(o->o.get)))
        return null;
    }
}
